/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tixclock;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 *
 * @author dwm77
 */
public class TIXGrid {
    private int gridValue = 0;
    private int gridSize = 0;
    private ArrayList allocated = null;
    private Calendar cal = null;
    private int i;
    private ArrayList<Color> colors = null;
    
    public TIXGrid(String gridId, Date date) {
        gridSize = 9;
        allocated = new ArrayList(gridSize);
        colors = new ArrayList<Color>();
        cal = Calendar.getInstance();
        cal.setTime(date);
        
        if(gridId.equals("A")) {
            gridValue =  Integer.parseInt(_formatDate("hh").substring(0, 1));
        }
        if(gridId.equals("B")) {
            gridValue =  Integer.parseInt(_formatDate("hh").substring(1, 2));
        }
        if(gridId.equals("C")) {
            gridValue =  Integer.parseInt(_formatDate("mm").substring(0, 1));
        }
        if(gridId.equals("D")) {
            gridValue =  Integer.parseInt(_formatDate("mm").substring(1, 2));
        }
        buildColors();
        allocate();
    }

    private String _formatDate(String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.format(cal.getTime());
        
    }

    private void allocate() {
        for(i=1; i<=gridValue; i++) {
            int squareToLight = getRandomPanel();
            while(allocated.contains(squareToLight)) {
                squareToLight = getRandomPanel();
            }
            allocated.add(squareToLight);
        }
    }

    public Boolean lightPanel(int panelId) {
        if(allocated.contains(panelId)) {
            return true;
        }else{
            return false;
        }
    }

    public Color lightPanelColor(int panelId) {
        if(lightPanel(panelId)) {
            return pickColor();
        }else{
            return new Color(255,255,255);
        }
    }

    private int getRandomPanel() {
        int x = getRandomNumber(gridSize);
        if(x == 0) {
            x = 1;
        }
        return x;

    }

    private int getRandomNumber(int max) {
        Random rand = new Random();
        return rand.nextInt(max + 1);
    }

    private Color getRandomColorHEX() {
        Color col = new Color(getRandomNumber(255),getRandomNumber(255),getRandomNumber(255));
        return col;
    }

    private void buildColors() {
        Color col1 = new Color(239,200,19);//yellow
        Color col2 = new Color(67,200,45); //green
        Color col3 = new Color(45,96,200); //blue
        Color col4 = new Color(239,39,19); //red

        colors.add(col1);
        colors.add(col2);
        colors.add(col3);
        colors.add(col4);

    }

    private Color pickColor() {
        int ranNum = getRandomNumber(colors.size());
        if(ranNum >= colors.size()) {
            ranNum--;
        }
        return colors.get(ranNum);
    }

}
