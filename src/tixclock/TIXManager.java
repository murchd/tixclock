/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tixclock;

import java.util.Date;

/**
 *
 * @author dwm77
 */
public class TIXManager {
    private static TIXManager instance = null;
    private Date date = null;
    public TIXManager() {
        date = new Date();
        System.out.println("Starting TIX Clock the time is: " + date.toString());
    }
    public TIXGrid getGrid(String gridId) {
        return new TIXGrid(gridId, date);
    }
    public static TIXManager getManager() {
       if(instance == null) {
         instance = new TIXManager();
      }
      return instance;

    }

    void setDate(Date date) {
        this.date = date;
    }
}
